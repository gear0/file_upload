# syntax=docker/dockerfile:1

##
## Build
##
FROM golang:1.18-alpine AS build

WORKDIR /app

COPY go.* ./

RUN go mod download

COPY *.go ./
# create a statically linked binary so it can be used in other stages without errors
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /file-upload

##
## Deploy
##
FROM ubuntu:22.04

WORKDIR /
RUN useradd -Ms /bin/sh nonroot

COPY --chown=nonroot:nonroot --from=build /file-upload /usr/local/bin/file-upload
COPY ./entrypoint.sh /entrypoint.sh

RUN mkdir /uploads && chown nonroot:nonroot /uploads
EXPOSE 8000

USER nonroot:nonroot

CMD [ "/usr/local/bin/file-upload" ]